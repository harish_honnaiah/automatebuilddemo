package com.mindtree.automatebuilddemo;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		System.out.println("Hello World!");
	}

	public String getGreeting() {
		return "Hello AutomateBuildDemo";
	}

	public int add(int x, int y) {
		return x + y;
	}
}
